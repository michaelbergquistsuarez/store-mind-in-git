# Store your mind in git
Because, why not?

# Theory
The concept is simple. Store your thoughts and ideas in git. This way, you can free your mind's resources for the more important stuff – happyness!

## Branches
Everything the the master branch should be understandable by the public.

# Solutions
## GitHub
Where I started out. There is no free privacy though.

## GitLab
Looks very interesting. Might be the best alternative.

# Tips and tricks
[Add your public SSH key](https://github.com/settings/keys) and then clone all repos with ssh. This way you don't need to enter your username and password every time you want to push. 
